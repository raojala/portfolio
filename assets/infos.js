function CallInfo(message) {
    var top_info = document.getElementById("top-info");
    top_info.innerHTML = message;
    top_info.style.zIndex = 1;
    top_info.style.opacity = 1;
    setTimeout(function(){
        document.getElementById('top-info').style.opacity = '0';
        }, 3000);
    setTimeout(function(){
        document.getElementById('top-info').style.zIndex = '0';
        }, 4000);
}

function CopyEmail() {
    const copyText = document.createElement('textarea');
    copyText.value = document.getElementById("email").innerHTML;
    document.body.appendChild(copyText);
    copyText.select();
    document.execCommand('copy');
    document.body.removeChild(copyText);
    copyText.select();
    copyText.setSelectionRange(0, 99999);
    document.execCommand("copy");
    CallInfo("Copied: " + copyText.value + " to clipboard.");
}