-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Store
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema Store
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Store` DEFAULT CHARACTER SET utf8 ;
USE `Store` ;

-- -----------------------------------------------------
-- Table `Store`.`Forum`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`Forum` (
  `ForumID` VARCHAR(37) NOT NULL,
  `Name` VARCHAR(80) NOT NULL,
  `Description` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`ForumID`),
  UNIQUE INDEX `Name_UNIQUE` (`Name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`ForumCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`ForumCategory` (
  `ForumCategoryID` VARCHAR(37) NOT NULL,
  `ForumID` VARCHAR(37) NOT NULL,
  `Name` VARCHAR(80) NOT NULL,
  `Description` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`ForumCategoryID`),
  INDEX `fk_Gategory_Forum_idx` (`ForumID` ASC) VISIBLE,
  CONSTRAINT `fk_Gategory_Forum`
    FOREIGN KEY (`ForumID`)
    REFERENCES `Store`.`Forum` (`ForumID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`User`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`User` (
  `UserID` VARCHAR(37) NOT NULL COMMENT 'UserID is trigger generated automatic identifier',
  `Username` VARCHAR(45) NOT NULL,
  `FirstName` VARCHAR(45) NOT NULL,
  `LastName` VARCHAR(45) NOT NULL,
  `Email` VARCHAR(120) NOT NULL,
  `Password` VARCHAR(256) NOT NULL,
  `Created` DATETIME NOT NULL,
  `IsDeleted` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`UserID`),
  UNIQUE INDEX `Email_UNIQUE` (`Email` ASC) VISIBLE,
  UNIQUE INDEX `Username_UNIQUE` (`Username` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`Message`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`Message` (
  `MessageID` VARCHAR(37) NOT NULL,
  `TopicID` VARCHAR(37) NOT NULL,
  `AuthorID` VARCHAR(37) NOT NULL,
  `MessageText` VARCHAR(1000) NOT NULL,
  `Posted` DATETIME NOT NULL,
  `Edited` DATETIME NULL,
  PRIMARY KEY (`MessageID`),
  INDEX `fk_Message_Topic1_idx` (`TopicID` ASC) VISIBLE,
  INDEX `fk_Message_User1_idx` (`AuthorID` ASC) VISIBLE,
  CONSTRAINT `fk_Message_Topic1`
    FOREIGN KEY (`TopicID`)
    REFERENCES `Store`.`Topic` (`TopicID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Message_User1`
    FOREIGN KEY (`AuthorID`)
    REFERENCES `Store`.`User` (`UserID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`Topic`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`Topic` (
  `TopicID` VARCHAR(37) NOT NULL,
  `ForumCategoryID` VARCHAR(37) NOT NULL,
  `AuthorMessageID` VARCHAR(37) NULL,
  `Title` VARCHAR(80) NOT NULL,
  `IsPinned` TINYINT NOT NULL DEFAULT 0,
  `Edited` DATETIME NULL,
  PRIMARY KEY (`TopicID`),
  INDEX `fk_Topic_Gategory1_idx` (`ForumCategoryID` ASC) VISIBLE,
  INDEX `fk_Topic_Message1_idx` (`AuthorMessageID` ASC) VISIBLE,
  CONSTRAINT `fk_Topic_Gategory1`
    FOREIGN KEY (`ForumCategoryID`)
    REFERENCES `Store`.`ForumCategory` (`ForumCategoryID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Topic_Message1`
    FOREIGN KEY (`AuthorMessageID`)
    REFERENCES `Store`.`Message` (`MessageID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`Privilege`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`Privilege` (
  `PrivilegeID` VARCHAR(37) NOT NULL,
  `Name` VARCHAR(80) NOT NULL,
  `Description` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`PrivilegeID`),
  UNIQUE INDEX `RoleName_UNIQUE` (`Name` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`UserPrivilege`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`UserPrivilege` (
  `UserPrivilegeID` VARCHAR(37) NOT NULL,
  `PrivilegeID` VARCHAR(37) NOT NULL,
  `UserID` VARCHAR(37) NOT NULL,
  `DateGranted` DATETIME NOT NULL,
  `DateRemoved` DATETIME NULL COMMENT 'is active if dateremoved == null\n',
  INDEX `fk_Role_has_User_User1_idx` (`UserID` ASC) VISIBLE,
  INDEX `fk_Role_has_User_Role1_idx` (`PrivilegeID` ASC) VISIBLE,
  PRIMARY KEY (`UserPrivilegeID`),
  CONSTRAINT `fk_Role_has_User_Role1`
    FOREIGN KEY (`PrivilegeID`)
    REFERENCES `Store`.`Privilege` (`PrivilegeID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Role_has_User_User1`
    FOREIGN KEY (`UserID`)
    REFERENCES `Store`.`User` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`ReportCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`ReportCategory` (
  `ReportCategoryID` VARCHAR(37) NOT NULL,
  `ReportCategoryName` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`ReportCategoryID`),
  UNIQUE INDEX `ReportCategoryName_UNIQUE` (`ReportCategoryName` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`Report`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`Report` (
  `ReportsID` VARCHAR(37) NOT NULL,
  `ReportCategoryID` VARCHAR(37) NOT NULL,
  `ReporterUserID` VARCHAR(37) NOT NULL,
  `ReportedUserID` VARCHAR(37) NULL,
  `Reason` VARCHAR(200) NOT NULL,
  `MoreInformation` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`ReportsID`),
  INDEX `fk_Report_ReportCategories1_idx` (`ReportCategoryID` ASC) VISIBLE,
  INDEX `fk_Report_User1_idx` (`ReporterUserID` ASC) VISIBLE,
  INDEX `fk_Report_User2_idx` (`ReportedUserID` ASC) VISIBLE,
  CONSTRAINT `fk_Report_ReportCategories1`
    FOREIGN KEY (`ReportCategoryID`)
    REFERENCES `Store`.`ReportCategory` (`ReportCategoryID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Report_User1`
    FOREIGN KEY (`ReporterUserID`)
    REFERENCES `Store`.`User` (`UserID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Report_User2`
    FOREIGN KEY (`ReportedUserID`)
    REFERENCES `Store`.`User` (`UserID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`Ban`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`Ban` (
  `BanID` VARCHAR(37) NOT NULL,
  `UserID` VARCHAR(37) NOT NULL,
  `DateGranted` DATETIME NOT NULL,
  `Reason` VARCHAR(500) NOT NULL,
  `DateOver` DATETIME NULL COMMENT 'PERMA BAN IF NULL',
  PRIMARY KEY (`BanID`),
  INDEX `fk_Ban_User1_idx` (`UserID` ASC) VISIBLE,
  CONSTRAINT `fk_Ban_User1`
    FOREIGN KEY (`UserID`)
    REFERENCES `Store`.`User` (`UserID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`UserWebSettings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`UserWebSettings` (
  `UserID` VARCHAR(37) NOT NULL,
  `Language` VARCHAR(45) NOT NULL DEFAULT 'ENG',
  `EmailAlertChangesToAccount` TINYINT NOT NULL DEFAULT 1,
  `EmailReceiveNewsAndOffers` TINYINT NOT NULL DEFAULT 0,
  `EmailIsVerified` TINYINT NOT NULL DEFAULT 0,
  `PrivacyPublicAccount` TINYINT NOT NULL DEFAULT 0,
  `PrivacyShareDataWithDevelopers` TINYINT NOT NULL DEFAULT 0,
  `AuthenticatorKey` VARCHAR(45) NULL,
  PRIMARY KEY (`UserID`),
  INDEX `fk_UserWebSettings_User1_idx` (`UserID` ASC) VISIBLE,
  UNIQUE INDEX `UserID_UNIQUE` (`UserID` ASC) VISIBLE,
  CONSTRAINT `fk_UserWebSettings_User1`
    FOREIGN KEY (`UserID`)
    REFERENCES `Store`.`User` (`UserID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`Service`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`Service` (
  `ServiceID` VARCHAR(37) NOT NULL,
  `ForumID` VARCHAR(37) NOT NULL,
  `ServiceName` VARCHAR(80) NOT NULL,
  `ServiceDescription` VARCHAR(500) NOT NULL,
  `PaymentLink` TEXT NULL,
  PRIMARY KEY (`ServiceID`),
  INDEX `fk_Service_Forum1_idx` (`ForumID` ASC) VISIBLE,
  CONSTRAINT `fk_Service_Forum1`
    FOREIGN KEY (`ForumID`)
    REFERENCES `Store`.`Forum` (`ForumID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`ServiceSubscription`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`ServiceSubscription` (
  `ServiceSubscriptionID` VARCHAR(37) NOT NULL,
  `ServiceID` VARCHAR(37) NOT NULL,
  `UserID` VARCHAR(37) NOT NULL,
  `DatePurchased` DATETIME NOT NULL,
  `DateOver` DATETIME NULL COMMENT 'Null DateOver marks ownership (non-sub)',
  PRIMARY KEY (`ServiceSubscriptionID`),
  INDEX `fk_GameSubscription_User1_idx` (`UserID` ASC) VISIBLE,
  INDEX `fk_ServiceSubscription_Service1_idx` (`ServiceID` ASC) VISIBLE,
  CONSTRAINT `fk_GameSubscription_User1`
    FOREIGN KEY (`UserID`)
    REFERENCES `Store`.`User` (`UserID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_ServiceSubscription_Service1`
    FOREIGN KEY (`ServiceID`)
    REFERENCES `Store`.`Service` (`ServiceID`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`LoginHistory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`LoginHistory` (
  `LoginHistoryID` VARCHAR(37) NOT NULL,
  `UserID` VARCHAR(37) NOT NULL,
  `Time` DATETIME NOT NULL,
  `IP` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`LoginHistoryID`),
  INDEX `fk_LoginHistory_User1_idx` (`UserID` ASC) VISIBLE,
  CONSTRAINT `fk_LoginHistory_User1`
    FOREIGN KEY (`UserID`)
    REFERENCES `Store`.`User` (`UserID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`ArticleCategory`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`ArticleCategory` (
  `ArticleCategoryID` VARCHAR(37) NOT NULL,
  `ArticleCategoryName` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`ArticleCategoryID`),
  UNIQUE INDEX `ArticleCategoryName_UNIQUE` (`ArticleCategoryName` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`Article`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`Article` (
  `ArticleID` VARCHAR(37) NOT NULL,
  `ArticleCategoryID` VARCHAR(37) NOT NULL,
  `AuthorUserID` VARCHAR(37) NOT NULL,
  `Posted` DATETIME NOT NULL,
  `Text` TEXT NOT NULL,
  `Edited` DATETIME NULL,
  PRIMARY KEY (`ArticleID`),
  INDEX `fk_Article_User1_idx` (`AuthorUserID` ASC) VISIBLE,
  INDEX `fk_Article_ArticleCategory1_idx` (`ArticleCategoryID` ASC) VISIBLE,
  CONSTRAINT `fk_Article_User1`
    FOREIGN KEY (`AuthorUserID`)
    REFERENCES `Store`.`User` (`UserID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Article_ArticleCategory1`
    FOREIGN KEY (`ArticleCategoryID`)
    REFERENCES `Store`.`ArticleCategory` (`ArticleCategoryID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`Product`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`Product` (
  `ProductID` VARCHAR(37) NOT NULL,
  `ForumID` VARCHAR(37) NOT NULL,
  `ProductName` VARCHAR(80) NOT NULL,
  `ProductDescription` VARCHAR(500) NOT NULL,
  `PaymentLink` TEXT NOT NULL,
  PRIMARY KEY (`ProductID`),
  INDEX `fk_Product_Forum1_idx` (`ForumID` ASC) VISIBLE,
  CONSTRAINT `fk_Product_Forum1`
    FOREIGN KEY (`ForumID`)
    REFERENCES `Store`.`Forum` (`ForumID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Store`.`ProductPurchase`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Store`.`ProductPurchase` (
  `ProductPurchaseID` VARCHAR(45) NOT NULL,
  `ProductID` VARCHAR(37) NOT NULL,
  `UserID` VARCHAR(37) NOT NULL,
  `DatePurchased` DATETIME NOT NULL,
  `IsRefunded` TINYINT NULL,
  INDEX `fk_Product_has_User_User1_idx` (`UserID` ASC) VISIBLE,
  INDEX `fk_Product_has_User_Product1_idx` (`ProductID` ASC) VISIBLE,
  PRIMARY KEY (`ProductPurchaseID`),
  CONSTRAINT `fk_Product_has_User_Product1`
    FOREIGN KEY (`ProductID`)
    REFERENCES `Store`.`Product` (`ProductID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Product_has_User_User1`
    FOREIGN KEY (`UserID`)
    REFERENCES `Store`.`User` (`UserID`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
-- begin attached script '1-procedures'
DELIMITER //

CREATE PROCEDURE getAllForums()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM Forum';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newForum(IN _Name VARCHAR(80), IN _Description VARCHAR(500))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @query = 'INSERT INTO Forum (ForumID, Name, Description) VALUES (?, ?, ?)';
        PREPARE stmt FROM @query;
        SET @p1 = _Name;
        SET @p2 = _Description;
        EXECUTE stmt USING @ID, @p1, @p2;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE editForum(IN _ForumID VARCHAR(37), IN _Name VARCHAR(80), IN _Description VARCHAR(500))
BEGIN
	START TRANSACTION;
        SET @FORUMID = _ForumID;
        SET @NAME = _Name;
        SET @DESCRIPTION = _Description;

        SET @query = 'UPDATE Forum SET Name=?, Description=? WHERE ForumID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @NAME, @DESCRIPTION, @FORUMID;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deleteForum(IN _ForumID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @query = 'DELETE FROM Forum WHERE ForumID=?';
        PREPARE stmt FROM @query;
        SET @p1 = _ForumID;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllServices()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM Service';
        SET @query = 'SELECT * FROM Service';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getService(IN _ServiceID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _ServiceID;
        SET @query = 'SELECT * FROM Service WHERE ServiceID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newService(IN _ServiceName VARCHAR(80), _ServiceDescription VARCHAR(500), IN _PaymentLink TEXT)
BEGIN
	START TRANSACTION;

        SET @PRODUCTNAME = _ServiceName;
        SET @SERVICEID = uuid();
        SET @FORUMID = uuid();
        SET @p1 = CONCAT(_ServiceName, " Forums");
        SET @p2 = CONCAT("Forum for ", _ServiceName);
        SET @p3 = _ServiceDescription;
        SET @p4 = _PaymentLink;

        SET @forumQuery = 'INSERT INTO Forum (ForumID, Name, Description) VALUES (?, ?, ?)';
        PREPARE stmt1 FROM @forumQuery;
        EXECUTE stmt1 USING @FORUMID, @p1, @p2;
        DEALLOCATE PREPARE stmt1;

        SET @productQuery = 'INSERT INTO Service (ServiceID, ForumID, ServiceName, ServiceDescription, PaymentLink) VALUES (?, ?, ?, ?, ?)';
        PREPARE stmt2 FROM @productQuery;
        EXECUTE stmt2 USING @SERVICEID, @FORUMID, @PRODUCTNAME, @p3, @p4;
        DEALLOCATE PREPARE stmt2;

        CALL defaultForumCategories(@FORUMID, @PRODUCTNAME);
	COMMIT;
END //

CREATE PROCEDURE editService(IN _ServiceID VARCHAR(37), IN _ForumID VARCHAR(37), IN _ServiceName VARCHAR(80), IN _ServiceDescription VARCHAR(500), IN _PaymentLink TEXT)
BEGIN
	START TRANSACTION;
        SET @SERVICEID = _ServiceID;
        SET @FORUMID = _ForumID;
        SET @SERVICENAME = _ServiceName;
        SET @SERVICEDESCRIPTION = _ServiceDescription;
        SET @PAYMENTLINK = _PaymentLink;

        SET @query = 'UPDATE Service SET ForumID=?, ServiceName=?, ServiceDescription=?, PaymentLink=? WHERE ServiceID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @FORUMID, @SERVICENAME, @SERVICEDESCRIPTION, @PAYMENTLINK, @SERVICEID;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deleteService(IN _ServiceID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @query = 'DELETE FROM Service WHERE ServiceID=?';
        PREPARE stmt FROM @query;
        SET @p1 = _ServiceID;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE searchService(IN _searchString TEXT )
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM Service WHERE ServiceName LIKE CONCAT("%", ?, "%")';
        PREPARE stmt FROM @query;
        SET @p1 = _searchString;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE searchProduct(IN _searchString TEXT )
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM Product WHERE ProductName LIKE CONCAT("%", ?, "%")';
        PREPARE stmt FROM @query;
        SET @p1 = _searchString;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getForumCategories(IN _ForumID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _ForumID;
        SET @query = 'SELECT * FROM ForumCategory WHERE ForumID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllForumCategories()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM ForumCategory';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newForumCategory(IN _ForumID VARCHAR(37), IN _Name VARCHAR(80), IN _Description VARCHAR(500))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @p1 = _ForumID;
        SET @p2 = _Name;
        SET @p3 = _Description;

        SET @query = 'INSERT INTO ForumCategory (ForumCategoryID, ForumID, Name, Description) VALUES (?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1 , @p2, @p3;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE editForumCategory(IN _ForumCategoryID VARCHAR(37), IN _ForumID VARCHAR(37), IN _Name VARCHAR(80), IN _Description VARCHAR(500))
BEGIN
	START TRANSACTION;
        SET @query = 'UPDATE ForumCategory SET ForumID=?, Name=?, Description=? WHERE ForumCategoryID=?';
        PREPARE stmt FROM @query;
        SET @p1 = _ForumID;
        SET @p2 = _Name;
        SET @p3 = _Description;
        SET @p4 = _ForumCategoryID;
        EXECUTE stmt USING @p1, @p2, @p3, @p4;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deleteForumCategory(IN _ForumCategoryID VARCHAR(37))
BEGIN
    START TRANSACTION;
        SET @query = 'DELETE FROM ForumCategory WHERE ForumCategoryID=?';
        PREPARE stmt FROM @query;
        SET @p1 = _ForumCategoryID;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getTopic(IN _ForumCategoryID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _ForumCategoryID;
        SET @query = 'SELECT * FROM Topic WHERE ForumCategoryID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllTopics()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM Topic';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE defaultForumCategories(IN _ForumID TEXT, IN _ProductName TEXT)
BEGIN
	START TRANSACTION;

        SET @FORUMID = _ForumID;
        SET @CATNAME1 = "General";
        SET @DESCRIPTION1 = CONCAT("General chat for ", _ProductName);
        CALL newForumCategory(@FORUMID , @CATNAME1, @DESCRIPTION1);

        SET @CATNAME2 = "Support";
        SET @DESCRIPTION2 = CONCAT("Support chat for ", _ProductName);
        CALL newForumCategory(@FORUMID , @CATNAME2, @DESCRIPTION2);

        SET @CATNAME3 = "Off-topic";
        SET @DESCRIPTION3 = CONCAT("Off-topic chat for ", _ProductName);
        CALL newForumCategory(@FORUMID , @CATNAME3, @DESCRIPTION3);

        SET @CATNAME4 = "Development";
        SET @DESCRIPTION4 = CONCAT("Development chat for ", _ProductName);
        CALL newForumCategory(@FORUMID , @CATNAME4, @DESCRIPTION4);
	COMMIT;
END //

CREATE PROCEDURE newTopic(IN _ForumCategoryID VARCHAR(37), IN _Title VARCHAR(80), IN _AuthorID VARCHAR(37), IN _Message VARCHAR(1000))
BEGIN
	START TRANSACTION;

        SET @_TopicID = uuid();
        SET @query1 = 'INSERT INTO Topic (TopicID, ForumCategoryID, Title) VALUES (?, ?, ?)';
        PREPARE stmt1 FROM @query1;
        SET @p1 = _ForumCategoryID;
        SET @p2 = _Title;
        EXECUTE stmt1 USING @_TopicID, @p1, @p2;
        DEALLOCATE PREPARE stmt1;

        SET @_MessageID = uuid();
        SET @POSTED = NOW();
        SET @query2 = 'INSERT INTO Message (MessageID, TopicID, AuthorID, MessageText, Posted) VALUES (?, ?, ?, ?, ?)';
        PREPARE stmt2 FROM @query2;
        SET @p3 = _AuthorID;
        SET @p4 = _Message;
        EXECUTE stmt2 USING @_MessageID, @_TopicID, @p3, @p4, @POSTED;
        DEALLOCATE PREPARE stmt2;

        SET @query3 = 'UPDATE Topic SET AuthorMessageID=? WHERE TopicID=?';
        PREPARE stmt3 FROM @query3;
        EXECUTE stmt3 USING @_MessageID, @_TopicID;
        DEALLOCATE PREPARE stmt3;
	COMMIT;
END //

CREATE PROCEDURE editTopic(IN _TopicID VARCHAR(37), IN _Title VARCHAR(80))
BEGIN
	START TRANSACTION;
        SET @EDITED = NOW();
        SET @query = 'UPDATE Topic SET Title=?, Edited=? WHERE TopicID=?';
        PREPARE stmt FROM @query;
        SET @p1 = _Title;
        SET @p2 = _TopicID;
        EXECUTE stmt USING @p1, @EDITED, @p2;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deleteTopic(IN _TopicID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _TopicID;

        SET @query = 'DELETE FROM Topic WHERE TopicID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE searchTopic(IN _searchString TEXT )
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM Topic WHERE Title LIKE CONCAT("%", ?, "%")';
        PREPARE stmt FROM @query;
        SET @p1 = _searchString;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE searchTopicFromCategory(IN _ForumCategoryID VARCHAR(37), IN _searchString TEXT)
BEGIN
	START TRANSACTION;
        SET @FORUMCATEGORYID = _ForumCategoryID; 
        SET @SEARCHSTRING = _searchString;

        SET @query = 'SELECT * FROM Topic WHERE ForumCategoryID=? AND Title LIKE CONCAT("%", ?, "%")';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @FORUMCATEGORYID, @SEARCHSTRING;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE pinTopic(IN _TopicID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @query = 'UPDATE Topic SET IsPinned=1 WHERE TopicID=?';
        PREPARE stmt FROM @query;
        SET @p1 = _TopicID;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllProducts()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM Product';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newProduct(IN _ProductName VARCHAR(80), IN _PaymentLink TEXT, _Description VARCHAR(500))
BEGIN
	START TRANSACTION;

        SET @PRODUCTNAME = _ProductName;
        SET @PRODUCTID = uuid();
        SET @FORUMID = uuid();
        SET @p1 = CONCAT(_ProductName, " Forums");
        SET @p2 = CONCAT("Forum for ", _ProductName);
        SET @p3 = _ProductName;
        SET @p4 = _Description;
        SET @p5 = _PaymentLink;

        SET @forumQuery = 'INSERT INTO Forum (ForumID, Name, Description) VALUES (?, ?, ?)';
        PREPARE stmt1 FROM @forumQuery;
        EXECUTE stmt1 USING @FORUMID, @p1, @p2;
        DEALLOCATE PREPARE stmt1;

        SET @productQuery = 'INSERT INTO Product (ProductID, ForumID, ProductName, ProductDescription, PaymentLink) VALUES (?, ?, ?, ?, ?)';
        PREPARE stmt2 FROM @productQuery;
        EXECUTE stmt2 USING @PRODUCTID, @FORUMID, @p3, @p4, @p5;
        DEALLOCATE PREPARE stmt2;

        CALL defaultForumCategories(@FORUMID, @PRODUCTNAME);

	COMMIT;
END //

CREATE PROCEDURE editProduct(IN _ProductID VARCHAR(37), IN _ForumID VARCHAR(37), IN _ProductName VARCHAR(80), IN _ProductDescription VARCHAR(500), IN _PaymentLink TEXT)
BEGIN
	START TRANSACTION;

        SET @p1 = _ForumID;
        SET @p2 = _ProductName;
        SET @p3 = _ProductDescription;
        SET @p4 = _PaymentLink;
        SET @p5 = _ProductID;
        
        SET @query = 'UPDATE Product SET ForumID=?, ProductName=?, ProductDescription=?, PaymentLink=? WHERE ProductID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1, @p2, @p3, @p4, @p5;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deleteProduct(IN _ProductID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _ProductID;
        SET @query = 'DELETE FROM Product WHERE ProductID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllProductPurchases()
BEGIN
	START TRANSACTION;

        SET @query = 'SELECT * FROM ProductPurchase';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getUserProductPurchaseHistory(IN _UserID TEXT)
BEGIN
	START TRANSACTION;
        SET @p1 = _UserID;

        SET @query = 'SELECT * FROM ProductPurchase WHERE UserID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getProductPurchaseHistory(IN _ProductID TEXT)
BEGIN
	START TRANSACTION;
        SET @p1 = _ProductID;

        SET @query = 'SELECT * FROM ProductPurchase WHERE ProductID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newProductPurchase(IN _ProductID VARCHAR(37), IN _UserID VARCHAR(37))
BEGIN
	START TRANSACTION;

        SET @PRODUCTPURCHASEID = uuid();
        SET @DATEPURCHASED = NOW();
        SET @p1 = _ProductID;
        SET @p2 = _UserID;

        SET @ProductPurchaseQuery = 'INSERT INTO ProductPurchase (ProductPurchaseID, ProductID, UserID, DatePurchased) VALUES (?, ?, ?, ?)';
        PREPARE stmt FROM @ProductPurchaseQuery;
        EXECUTE stmt USING @PRODUCTPURCHASEID, @p1, @p2, @DATEPURCHASED;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE refundProductPurchase(IN _ProductPurchaseID TEXT)
BEGIN
	START TRANSACTION;
        SET @p1 = _ProductPurchaseID;

        SET @query = 'UPDATE ProductPurchase SET IsRefunded=1 WHERE ProductPurchaseID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllMessages()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM Message';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getTopicMessages(IN _TopicID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _TopicID;
        SET @query = 'SELECT * FROM Message WHERE TopicID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newMessage(IN _TopicID VARCHAR(37), IN _AuthorID VARCHAR(37), IN _Message VARCHAR(1000))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @POSTED = NOW();
        SET @p1 = _TopicID;
        SET @p2 = _AuthorID;
        SET @p3 = _Message;

        SET @query = 'INSERT INTO Message (MessageID, TopicID, AuthorID, MessageText, Posted) VALUES (?, ?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1, @p2, @p3, @POSTED;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE editMessage(IN _MessageID VARCHAR(37), IN _MessageText VARCHAR(1000))
BEGIN
	START TRANSACTION;
        SET @EDITED = NOW();
        SET @p1 = _MessageText;
        SET @p2 = _MessageID;
        
        SET @query = 'UPDATE Message SET MessageText=?, Edited=? WHERE MessageID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1, @EDITED, @p2;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deleteMessage(IN _MessageID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _MessageID;

        SET @query = 'DELETE FROM Message WHERE MessageID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE searchMessage(IN _searchString TEXT)
BEGIN
	START TRANSACTION;
        SET @p1 = _searchString;

        SET @query = 'SELECT * FROM Message WHERE MessageText LIKE CONCAT("%", ?, "%")';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE searchMessageFromTopic(IN _searchString TEXT, IN _topicID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @TOPICID = _topicID;
        SET @SEARCHSTRING = _searchString;

        SET @query = 'SELECT * FROM Message WHERE TopicID=? AND MessageText LIKE CONCAT("%", ?, "%")';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @TOPICID, @SEARCHSTRING;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getServiceSubscriptionHistory(IN _UserID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _UserID;

        SET @query = 'SELECT * FROM ServiceSubscription WHERE UserID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newServiceSubscription(IN _ServiceID VARCHAR(37), IN _UserID VARCHAR(37), IN _Days INT)
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @DATEPURCHASED = NOW();
        SET @DATEOVER = DATE_ADD(@DATEPURCHASED, INTERVAL _Days DAY);
        SET @p1 = _ServiceID;
        SET @p2 = _UserID;

        SET @query = 'INSERT INTO ServiceSubscription (ServiceSubscriptionID, ServiceID, UserID, DatePurchased, DateOver) VALUES (?, ?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1, @p2, @DATEPURCHASED, @DATEOVER;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newLifeTimeServiceSubscription(IN _ServiceID VARCHAR(37), IN _UserID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @DATEPURCHASED = NOW();
        SET @p1 = _ServiceID;
        SET @p2 = _UserID;

        SET @query = 'INSERT INTO ServiceSubscription (ServiceSubscriptionID, ServiceID, UserID, DatePurchased) VALUES (?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1, @p2, @DATEPURCHASED;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllReportCategories()
BEGIN
	START TRANSACTION;

        SET @query = 'SELECT * FROM ReportCategory';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newReportCategory(IN _ReportCategoryName VARCHAR(80))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @p1 = _ReportCategoryName;

        SET @query = 'INSERT INTO ReportCategory (ReportCategoryID, ReportCategoryName) VALUES (?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE editReportCategory(IN _ReportCategoryID VARCHAR(37), IN _ReportCategoryName VARCHAR(80))
BEGIN
	START TRANSACTION;
        SET @query = 'UPDATE ReportCategory SET ReportCategoryName=? WHERE ReportCategoryID=?';
        PREPARE stmt FROM @query;
        SET @p1 = _ReportCategoryName;
        SET @p2 = _ReportCategoryID;
        EXECUTE stmt USING @p1, @p2;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deleteReportCategory(IN _ReportCategoryID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _ReportCategoryID;

        SET @query = 'DELETE FROM ReportCategory WHERE ReportCategoryID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllReports()
BEGIN
	START TRANSACTION;

        SET @query = 'SELECT * FROM Report';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newReport(IN _ReportCategoryID VARCHAR(37), IN _ReporterUserID VARCHAR(37), IN _Reason VARCHAR(200), IN _MoreInformation VARCHAR(500))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @query = 'INSERT INTO Report (ReportsID, ReportCategoryID, ReporterUserID, Reason, MoreInformation) VALUES (?, ?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        SET @p1 = _ReportCategoryID;
        SET @p2 = _ReporterUserID;
        SET @p3 = _Reason;
        SET @p4 = _MoreInformation;
        EXECUTE stmt USING @ID, @p1, @p2, @p3, @p4;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newReportUser(IN _ReportCategoryID VARCHAR(37), IN _ReporterUserID VARCHAR(37), IN _ReportedUserID VARCHAR(37), IN _Reason VARCHAR(200), IN _MoreInformation VARCHAR(500))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @query = 'INSERT INTO Report (ReportsID, ReportCategoryID, ReporterUserID, ReportedUserID, Reason, MoreInformation) VALUES (?, ?, ?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        SET @p1 = _ReportCategoryID;
        SET @p2 = _ReporterUserID;
        SET @p3 = _ReportedUserID;
        SET @p4 = _Reason;
        SET @p5 = _MoreInformation;
        EXECUTE stmt USING @ID, @p1, @p2, @p3, @p4, @p5;
        DEALLOCATE PREPARE stmt; 
	COMMIT;
END //

CREATE PROCEDURE getBanLog()
BEGIN
	START TRANSACTION;

        SET @query = 'SELECT * FROM Ban';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getUserBanHistory(IN _UserID VARCHAR(37))
BEGIN
	START TRANSACTION;

        SET @p1 = _UserID;
        SET @query = 'SELECT BanID, DateGranted, DateOver FROM Ban WHERE UserID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newTempBan(IN _UserID VARCHAR(37), IN _Reason VARCHAR(500), IN _Days INT)
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @DATEGRANTED = NOW();
        SET @DATEOVER = DATE_ADD(@DATEGRANTED, INTERVAL _Days DAY);
        SET @p1 = _UserID;
        SET @p2 = _Reason;

        SET @query = 'INSERT INTO Ban (BanID, UserID, DateGranted, DateOver, Reason) VALUES (?, ?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1, @DATEGRANTED, @DATEOVER, @p2;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newPermaBan(IN _UserID VARCHAR(37), IN _Reason VARCHAR(500))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @DATEGRANTED = NOW();
        SET @p1 = _UserID;
        SET @p2 = _Reason;

        SET @query = 'INSERT INTO Ban (BanID, UserID, DateGranted, Reason) VALUES (?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1, @DATEGRANTED, @p2;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deleteBan(IN _BanID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _BanID;

        SET @query = 'DELETE FROM Ban WHERE BanID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getLoginHistory(IN _UserID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _UserID;

        SET @query = 'SELECT * FROM LoginHistory WHERE UserID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newLoginHistory(IN _UserID VARCHAR(37), IN _IP VARCHAR(45))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @DATELOGGED = NOW();
        SET @p1 = _UserID;
        SET @p2 = _IP;

        SET @query = 'INSERT INTO LoginHistory (LoginHistoryID, UserID, Time, IP) VALUES (?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1, @DATELOGGED, @p2;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllUsers()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM User';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newUser(IN _Username VARCHAR(45), IN _FirstName VARCHAR(45), IN _LastName VARCHAR(45), IN _Email VARCHAR(120), IN _Password VARCHAR(256))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @CREATED = NOW();
        SET @p1 = _Username;
        SET @p2 = _FirstName;
        SET @p3 = _LastName;
        SET @p4 = _Email;
        SET @p5 = _Password;

        SET @query = 'INSERT INTO User (UserID, Username, FirstName, LastName, Email, Password, Created) VALUES (?, ?, ?, ?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1, @p2, @p3, @p4, @p5, @CREATED;
        DEALLOCATE PREPARE stmt;

        SET @query2 = 'INSERT INTO UserWebSettings (UserID) VALUES (?)';
        PREPARE stmt2 FROM @query2;
        EXECUTE stmt2 USING @ID;
        DEALLOCATE PREPARE stmt2;
	COMMIT;
END //

CREATE PROCEDURE editUser(IN _UserID VARCHAR(37), IN _FirstName VARCHAR(45), IN _LastName VARCHAR(45), IN _Email VARCHAR(120), IN _Password VARCHAR(256))
BEGIN
	START TRANSACTION;
        SET @USERID = _UserID;
        SET @FIRSTNAME = _FirstName;
        SET @LASTNAME = _LastName;
        SET @EMAIL = _Email;
        SET @PASSWORD = _Password;

        SET @query = 'UPDATE User SET FirstName=?, LastName=?, Email=?, Password=? WHERE UserID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @FIRSTNAME, @LASTNAME, @EMAIL, @PASSWORD, @USERID;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE disableUser(IN _UserID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @query = 'UPDATE User SET IsDeleted=1 WHERE UserID=?';
        PREPARE stmt FROM @query;
        SET @p1 = _UserID;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
        CALL obscurifyUser(@p1);
	COMMIT;
END //

CREATE PROCEDURE obscurifyUser(IN _UserID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @query = 'UPDATE User SET FirstName="Redacted", LastName="Redacted", Password="Redacted" WHERE UserID=?';
        PREPARE stmt FROM @query;
        SET @p1 = _UserID;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllUserWebSettings()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM UserWebSettings';
        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getUserWebSettings(IN _UserID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _UserID;
        SET @query = 'SELECT * FROM UserWebSettings WHERE UserID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE editUserWebSettings(IN _UserID VARCHAR(37), IN _Language VARCHAR(45), IN _EmailAlertChangesToAccount BOOLEAN, IN _EmailReceiveNewsAndOffers BOOLEAN, IN _EmailIsVerified BOOLEAN, IN _PrivacyPublicAccount BOOLEAN, IN _PrivacyShareDataWithDevelopers BOOLEAN, IN _AuthenticatorKey VARCHAR(45))
BEGIN
	START TRANSACTION;
        SET @LANGUAGE = _Language;
        SET @EMAILALERTCHANGESTOACCOUNT = _EmailAlertChangesToAccount;
        SET @EMAILRECEIVENEWSANDOFFERS = _EmailReceiveNewsAndOffers;
        SET @EMAILISVERIFIED = _EmailIsVerified;
        SET @PRIVACYPUBLICACCOUNT = _PrivacyPublicAccount;
        SET @PRIVACYSHAREDATAWITHDEVELOPERS = _PrivacyShareDataWithDevelopers;
        SET @AUTHENTICATORKEY = _AuthenticatorKey;
        SET @USERID = _UserID;

        SET @query = 'UPDATE UserWebSettings SET Language=?, EmailAlertChangesToAccount=?, EmailReceiveNewsAndOffers=?, EmailIsVerified=?, PrivacyPublicAccount=?, PrivacyShareDataWithDevelopers=?, AuthenticatorKey=? WHERE UserID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @LANGUAGE, @EMAILALERTCHANGESTOACCOUNT, @EMAILRECEIVENEWSANDOFFERS, @EMAILISVERIFIED, @PRIVACYPUBLICACCOUNT, @PRIVACYSHAREDATAWITHDEVELOPERS, @AUTHENTICATORKEY, @USERID;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllPrivileges()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM Privilege';

        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newPrivilege(IN _Name VARCHAR(80), IN _Description VARCHAR(500))
BEGIN
	START TRANSACTION;

        SET @ID = uuid();
        SET @p1 = _Name;
        SET @p2 = _Description;

        SET @query = 'INSERT INTO Privilege (PrivilegeID, Name, Description) VALUES (?, ?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1, @p2;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE editPrivilege(IN _PrivilegeID VARCHAR(37), IN _Name VARCHAR(45), IN _Description VARCHAR(500))
BEGIN
	START TRANSACTION;
        SET @p1 = _Name;
        SET @p2 = _Description;
        SET @p3 = _PrivilegeID;

        SET @query = 'UPDATE Privilege SET Name=?, Description=? WHERE PrivilegeID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1, @p2, @p3;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deletePrivilege(IN _PrivilegeID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _PrivilegeID;

        SET @query = 'DELETE FROM Privilege WHERE PrivilegeID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getUserPrivileges(IN _UserID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _UserID;

        SET @query = '  SELECT *
                        FROM UserPrivilege
                        INNER JOIN Privilege
                        ON UserPrivilege.PrivilegeID=Privilege.PrivilegeID
                        WHERE UserPrivilege.UserID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getPrivilegeUsers(IN _PrivilegeID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _PrivilegeID;

        SET @query = '  SELECT *
                        FROM UserPrivilege
                        INNER JOIN Privilege
                        ON UserPrivilege.PrivilegeID=Privilege.PrivilegeID
                        WHERE Privilege.PrivilegeID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newUserPrivilege(IN _PrivilegeID VARCHAR(37), IN _UserID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @DATEGRANTED = NOW();
        SET @p1 = _PrivilegeID;
        SET @p2 = _UserID;

        SET @query = 'INSERT INTO UserPrivilege (UserPrivilegeID, PrivilegeID, UserID, DateGranted) VALUES (?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1, @p2, @DATEGRANTED;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE revokeUserPrivilege(IN _UserID VARCHAR(37), IN _UserPrivilegeID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @DATEREMOVED = NOW();
        SET @USERID = _UserID;
        SET @USERPRIVILEGEID = _UserPrivilegeID;

        SET @query = 'UPDATE UserPrivilege SET DateRemoved=? WHERE UserID=? AND UserPrivilegeID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @DATEREMOVED, @USERID, @USERPRIVILEGEID;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllArticleCategories()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM ArticleCategory';

        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newArticleCategory(IN _Name VARCHAR(45))
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @p1 = _Name;

        SET @query = 'INSERT INTO ArticleCategory (ArticleCategoryID, ArticleCategoryName) VALUES (?, ?)';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @ID, @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE editArticleCategory(IN _ArticleCategoryID VARCHAR(37), IN _ArticleCategoryName VARCHAR(45))
BEGIN
	START TRANSACTION;
        SET @p1 = _ArticleCategoryName;
        SET @p2 = _ArticleCategoryID;

        SET @query = 'UPDATE ArticleCategory SET ArticleCategoryName=? WHERE ArticleCategoryID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING  @p1, @p2;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deleteArticleCategory(IN _ArticleCategoryID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _ArticleCategoryID;

        SET @query = 'DELETE FROM ArticleCategory WHERE ArticleCategoryID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE getAllArticles()
BEGIN
	START TRANSACTION;
        SET @query = 'SELECT * FROM Article';

        PREPARE stmt FROM @query;
        EXECUTE stmt;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE newArticle(IN _ArticleCategoryID VARCHAR(37), IN _AuthorUserID VARCHAR(37), IN _Text TEXT)
BEGIN
	START TRANSACTION;
        SET @ID = uuid();
        SET @POSTED = NOW();
        SET @query = 'INSERT INTO Article (ArticleID, ArticleCategoryID, AuthorUserID, Posted, Text) VALUES (?, ?, ?, ?, ?)';
        PREPARE stmt FROM @query;
        SET @p1 = _ArticleCategoryID;
        SET @p2 = _AuthorUserID;
        SET @p3 = _Text;
        EXECUTE stmt USING @ID, @p1, @p2, @POSTED, @p3;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE editArticle(IN _ArticleID VARCHAR(37), IN _ArticleCategoryID VARCHAR(37), IN _Text TEXT)
BEGIN
	START TRANSACTION;
        SET @EDITED = NOW();
        SET @query = 'UPDATE Article SET Text=?, Edited=?, ArticleCategoryID=? WHERE ArticleID=?';
        PREPARE stmt FROM @query;
        SET @p1 = _Text;
        SET @p2 = _ArticleCategoryID;
        SET @p3 = _ArticleID;
        EXECUTE stmt USING @p1, @EDITED, @p2, @p3;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

CREATE PROCEDURE deleteArticle(IN _ArticleID VARCHAR(37))
BEGIN
	START TRANSACTION;
        SET @p1 = _ArticleID;

        SET @query = 'DELETE FROM Article WHERE ArticleID=?';
        PREPARE stmt FROM @query;
        EXECUTE stmt USING @p1;
        DEALLOCATE PREPARE stmt;
	COMMIT;
END //

DELIMITER ;
-- end attached script '1-procedures'
-- begin attached script '2-usersAndPermissions'
DROP USER IF EXISTS 'Admin'@'localhost';
CREATE USER 'Admin'@'localhost' IDENTIFIED BY 'password';
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'Admin'@'localhost';

DROP USER IF EXISTS 'User'@'*';
CREATE USER 'User'@'*' IDENTIFIED BY 'password';
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'User'@'*';

DROP USER IF EXISTS 'Reporter'@'*';
CREATE USER 'Reporter'@'*' IDENTIFIED BY 'password';
REVOKE ALL PRIVILEGES, GRANT OPTION FROM 'Reporter'@'*';

GRANT EXECUTE ON PROCEDURE defaultForumCategories TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deleteArticle TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deleteArticleCategory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deleteBan TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deleteForum TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deleteForumCategory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deleteMessage TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deletePrivilege TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deleteProduct TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deleteReportCategory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deleteService TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE deleteTopic TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE disableUser TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editArticle TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editArticleCategory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editForum TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editForumCategory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editMessage TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editPrivilege TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editProduct TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editReportCategory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editService TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editTopic TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editUser TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE editUserWebSettings TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllArticleCategories TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllArticles TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllForumCategories TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllForums TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllMessages TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllPrivileges TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllProductPurchases TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllProducts TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllReportCategories TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllReports TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllServices TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllTopics TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllUsers TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getAllUserWebSettings TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getBanLog TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getForumCategories TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getLoginHistory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getPrivilegeUsers TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getProductPurchaseHistory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getService TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getServiceSubscriptionHistory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getTopic TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getTopicMessages TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getUserBanHistory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getUserPrivileges TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getUserProductPurchaseHistory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE getUserWebSettings TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newArticle TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newArticleCategory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newForum TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newForumCategory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newLifeTimeServiceSubscription TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newLoginHistory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newMessage TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newPermaBan TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newPrivilege TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newProduct TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newProductPurchase TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newReport TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newReportCategory TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newReportUser TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newService TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newServiceSubscription TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newTempBan TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newTopic TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newUser TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE newUserPrivilege TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE obscurifyUser TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE pinTopic TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE refundProductPurchase TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE revokeUserPrivilege TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE searchMessage TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE searchMessageFromTopic TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE searchProduct TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE searchService TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE searchTopic TO 'Admin'@'localhost';
GRANT EXECUTE ON PROCEDURE searchTopicFromCategory TO 'Admin'@'localhost';

GRANT EXECUTE ON PROCEDURE deleteMessage TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE editMessage TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE editUser TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE editUserWebSettings TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllArticleCategories TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllArticles TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllForumCategories TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllForums TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllMessages TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllPrivileges TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllProductPurchases TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllProducts TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllReportCategories TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllReports TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllServices TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllTopics TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllUsers TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getAllUserWebSettings TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getBanLog TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getForumCategories TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getLoginHistory TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getPrivilegeUsers TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getProductPurchaseHistory TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getService TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getServiceSubscriptionHistory TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getTopic TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getTopicMessages TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getUserBanHistory TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getUserPrivileges TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getUserProductPurchaseHistory TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE getUserWebSettings TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE newMessage TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE newReport TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE newReportUser TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE newTopic TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE searchMessage TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE searchMessageFromTopic TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE searchProduct TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE searchService TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE searchTopic TO 'User'@'*';
GRANT EXECUTE ON PROCEDURE searchTopicFromCategory TO 'User'@'*';

GRANT EXECUTE ON PROCEDURE deleteArticle TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE deleteMessage TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE deleteTopic TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE editArticle TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE editForum TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE editMessage TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE editTopic TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE editUser TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE editUserWebSettings TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllArticleCategories TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllArticles TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllForumCategories TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllForums TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllMessages TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllPrivileges TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllProductPurchases TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllProducts TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllReportCategories TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllReports TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllServices TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllTopics TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllUsers TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getAllUserWebSettings TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getBanLog TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getForumCategories TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getLoginHistory TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getPrivilegeUsers TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getProductPurchaseHistory TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getService TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getServiceSubscriptionHistory TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getTopic TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getTopicMessages TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getUserBanHistory TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getUserPrivileges TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getUserProductPurchaseHistory TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE getUserWebSettings TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE newArticle TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE newForum TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE newReport TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE newReportUser TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE newTempBan TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE newTopic TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE pinTopic TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE searchMessage TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE searchMessageFromTopic TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE searchProduct TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE searchService TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE searchTopic TO 'Reporter'@'*';
GRANT EXECUTE ON PROCEDURE searchTopicFromCategory TO 'Reporter'@'*';
-- end attached script '2-usersAndPermissions'
