-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema universedb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema universedb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `universedb` DEFAULT CHARACTER SET utf8 ;
USE `universedb` ;

-- -----------------------------------------------------
-- Table `universedb`.`filamenttypes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`filamenttypes` (
  `idfilamenttypes` INT(11) NOT NULL AUTO_INCREMENT,
  `subname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idfilamenttypes`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`filament`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`filament` (
  `idfilament` INT(11) NOT NULL AUTO_INCREMENT,
  `filamentname` VARCHAR(45) NOT NULL,
  `idfilamenttypes` INT(11) NOT NULL,
  PRIMARY KEY (`idfilament`),
  INDEX `fk_filament_filamenttypes1_idx` (`idfilamenttypes` ASC),
  CONSTRAINT `fk_filament_filamenttypes1`
    FOREIGN KEY (`idfilamenttypes`)
    REFERENCES `universedb`.`filamenttypes` (`idfilamenttypes`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`supercluster`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`supercluster` (
  `idsupercluster` INT(11) NOT NULL AUTO_INCREMENT,
  `clustername` VARCHAR(45) NOT NULL,
  `lengthmly` INT(11) NULL DEFAULT NULL,
  `idfilament` INT(11) NOT NULL,
  PRIMARY KEY (`idsupercluster`),
  INDEX `fk_supercluster_filament1_idx` (`idfilament` ASC),
  CONSTRAINT `fk_supercluster_filament1`
    FOREIGN KEY (`idfilament`)
    REFERENCES `universedb`.`filament` (`idfilament`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`galaxygroup`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`galaxygroup` (
  `idgalaxygroup` BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `galaxygroupname` VARCHAR(45) NOT NULL,
  `idsupercluster` INT(11) NOT NULL,
  PRIMARY KEY (`idgalaxygroup`),
  INDEX `fk_galaxygroup_supercluster1_idx` (`idsupercluster` ASC),
  CONSTRAINT `fk_galaxygroup_supercluster1`
    FOREIGN KEY (`idsupercluster`)
    REFERENCES `universedb`.`supercluster` (`idsupercluster`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`galaxytype`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`galaxytype` (
  `idgalaxytype` INT(11) NOT NULL AUTO_INCREMENT,
  `galaxytypename` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idgalaxytype`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`galaxy`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`galaxy` (
  `idgalaxy` BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `galaxyname` VARCHAR(80) NOT NULL,
  `center` VARCHAR(80) NULL DEFAULT NULL,
  `diameterkly` BIGINT(11) UNSIGNED NULL DEFAULT NULL,
  `idgalaxygroup` BIGINT(11) UNSIGNED NOT NULL,
  `idgalaxytype` INT(11) NOT NULL,
  PRIMARY KEY (`idgalaxy`),
  INDEX `fk_galaxy_galaxygroup1_idx` (`idgalaxygroup` ASC),
  INDEX `fk_galaxy_galaxytype1_idx` (`idgalaxytype` ASC),
  CONSTRAINT `fk_galaxy_galaxygroup1`
    FOREIGN KEY (`idgalaxygroup`)
    REFERENCES `universedb`.`galaxygroup` (`idgalaxygroup`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_galaxy_galaxytype1`
    FOREIGN KEY (`idgalaxytype`)
    REFERENCES `universedb`.`galaxytype` (`idgalaxytype`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`nebulatype`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`nebulatype` (
  `idnebulatype` INT(11) NOT NULL AUTO_INCREMENT,
  `nebulatypename` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idnebulatype`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`nebula`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`nebula` (
  `idnebula` INT NOT NULL AUTO_INCREMENT,
  `idnebulatype` INT(11) NOT NULL,
  `nebulaname` VARCHAR(45) NOT NULL,
  `idgalaxy` BIGINT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`idnebula`),
  INDEX `fk_nebula_nebulatype1_idx` (`idnebulatype` ASC),
  INDEX `fk_nebula_galaxy1_idx` (`idgalaxy` ASC),
  CONSTRAINT `fk_nebula_nebulatype1`
    FOREIGN KEY (`idnebulatype`)
    REFERENCES `universedb`.`nebulatype` (`idnebulatype`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_nebula_galaxy1`
    FOREIGN KEY (`idgalaxy`)
    REFERENCES `universedb`.`galaxy` (`idgalaxy`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`startype`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`startype` (
  `idstartype` TINYINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `typename` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`idstartype`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`solarsystem`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`solarsystem` (
  `idsolarsystem` BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `systemname` VARCHAR(80) NOT NULL,
  `agebiy` BIGINT(11) UNSIGNED NULL DEFAULT NULL,
  `diameterau` FLOAT UNSIGNED NULL DEFAULT NULL,
  `idgalaxy` BIGINT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`idsolarsystem`),
  INDEX `fk_solarsystem_galaxy1_idx` (`idgalaxy` ASC),
  CONSTRAINT `fk_solarsystem_galaxy1`
    FOREIGN KEY (`idgalaxy`)
    REFERENCES `universedb`.`galaxy` (`idgalaxy`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`star`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`star` (
  `idstar` BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `starname` VARCHAR(80) NOT NULL,
  `mass` DOUBLE UNSIGNED NULL DEFAULT NULL,
  `luminosity` DOUBLE UNSIGNED NULL DEFAULT NULL,
  `temperature` FLOAT NULL DEFAULT NULL,
  `agebiy` BIGINT(11) UNSIGNED NULL DEFAULT NULL,
  `diameter` FLOAT UNSIGNED NULL DEFAULT NULL,
  `idstartype` TINYINT(11) UNSIGNED NOT NULL,
  `idsolarsystem` BIGINT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`idstar`),
  INDEX `fk_star_startype1_idx` (`idstartype` ASC),
  INDEX `fk_star_solarsystem1_idx` (`idsolarsystem` ASC),
  CONSTRAINT `fk_star_startype1`
    FOREIGN KEY (`idstartype`)
    REFERENCES `universedb`.`startype` (`idstartype`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_star_solarsystem1`
    FOREIGN KEY (`idsolarsystem`)
    REFERENCES `universedb`.`solarsystem` (`idsolarsystem`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`orbitalbody_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`orbitalbody_type` (
  `idorbitalbody_type` TINYINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `typename` VARCHAR(80) NOT NULL,
  PRIMARY KEY (`idorbitalbody_type`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`orbitalbody`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`orbitalbody` (
  `idorbitalbody` BIGINT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `bodyname` VARCHAR(80) NOT NULL,
  `mass` DOUBLE UNSIGNED NULL DEFAULT NULL,
  `diameter` DOUBLE UNSIGNED NULL DEFAULT NULL,
  `au_to_parent` DOUBLE UNSIGNED NULL,
  `km_to_parent` DOUBLE UNSIGNED NULL,
  `idstar` BIGINT(11) UNSIGNED NULL,
  `parent_idorbitalbody` BIGINT(11) UNSIGNED NULL,
  `idorbitalbody_type` TINYINT(11) UNSIGNED NOT NULL,
  PRIMARY KEY (`idorbitalbody`),
  INDEX `fk_orbitalbody_star1_idx` (`idstar` ASC),
  INDEX `fk_orbitalbody_orbitalbody1_idx` (`parent_idorbitalbody` ASC),
  INDEX `fk_orbitalbody_orbitalbody_type1_idx` (`idorbitalbody_type` ASC),
  CONSTRAINT `fk_orbitalbody_star1`
    FOREIGN KEY (`idstar`)
    REFERENCES `universedb`.`star` (`idstar`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orbitalbody_orbitalbody1`
    FOREIGN KEY (`parent_idorbitalbody`)
    REFERENCES `universedb`.`orbitalbody` (`idorbitalbody`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_orbitalbody_orbitalbody_type1`
    FOREIGN KEY (`idorbitalbody_type`)
    REFERENCES `universedb`.`orbitalbody_type` (`idorbitalbody_type`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`periodictable_elements`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`periodictable_elements` (
  `periodicnumber` TINYINT(11) NOT NULL,
  `symbol` VARCHAR(45) NOT NULL,
  `elementname` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`periodicnumber`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`nebula_elements`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`nebula_elements` (
  `periodicnumber` TINYINT(11) NOT NULL,
  `idnebula` INT NOT NULL,
  `percentile` FLOAT NOT NULL,
  PRIMARY KEY (`periodicnumber`, `idnebula`),
  INDEX `fk_periodictable_elements_has_nebula_nebula1_idx` (`idnebula` ASC),
  INDEX `fk_periodictable_elements_has_nebula_periodictable_elements_idx` (`periodicnumber` ASC),
  CONSTRAINT `fk_periodictable_elements_has_nebula_periodictable_elements1`
    FOREIGN KEY (`periodicnumber`)
    REFERENCES `universedb`.`periodictable_elements` (`periodicnumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_periodictable_elements_has_nebula_nebula1`
    FOREIGN KEY (`idnebula`)
    REFERENCES `universedb`.`nebula` (`idnebula`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`orbitalbody_elements`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`orbitalbody_elements` (
  `periodicnumber` TINYINT(11) NOT NULL,
  `idorbitalbody` BIGINT(11) UNSIGNED NOT NULL,
  `percentile` FLOAT NOT NULL,
  PRIMARY KEY (`periodicnumber`, `idorbitalbody`),
  INDEX `fk_periodictable_elements_has_orbitalbody_orbitalbody1_idx` (`idorbitalbody` ASC),
  INDEX `fk_periodictable_elements_has_orbitalbody_periodictable_ele_idx` (`periodicnumber` ASC),
  CONSTRAINT `fk_periodictable_elements_has_orbitalbody_periodictable_eleme1`
    FOREIGN KEY (`periodicnumber`)
    REFERENCES `universedb`.`periodictable_elements` (`periodicnumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_periodictable_elements_has_orbitalbody_orbitalbody1`
    FOREIGN KEY (`idorbitalbody`)
    REFERENCES `universedb`.`orbitalbody` (`idorbitalbody`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`atmosphere`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`atmosphere` (
  `periodicnumber` TINYINT(11) NOT NULL,
  `idorbitalbody` BIGINT(11) UNSIGNED NOT NULL,
  `percentile` FLOAT NOT NULL,
  PRIMARY KEY (`periodicnumber`, `idorbitalbody`),
  INDEX `fk_periodictable_elements_has_orbitalbody_orbitalbody2_idx` (`idorbitalbody` ASC),
  INDEX `fk_periodictable_elements_has_orbitalbody_periodictable_ele_idx` (`periodicnumber` ASC),
  CONSTRAINT `fk_periodictable_elements_has_orbitalbody_periodictable_eleme2`
    FOREIGN KEY (`periodicnumber`)
    REFERENCES `universedb`.`periodictable_elements` (`periodicnumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_periodictable_elements_has_orbitalbody_orbitalbody2`
    FOREIGN KEY (`idorbitalbody`)
    REFERENCES `universedb`.`orbitalbody` (`idorbitalbody`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `universedb`.`star_elements`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `universedb`.`star_elements` (
  `periodicnumber` TINYINT(11) NOT NULL,
  `idstar` BIGINT(11) UNSIGNED NOT NULL,
  `percentile` FLOAT NOT NULL,
  PRIMARY KEY (`periodicnumber`, `idstar`),
  INDEX `fk_periodictable_elements_has_star_star1_idx` (`idstar` ASC),
  INDEX `fk_periodictable_elements_has_star_periodictable_elements1_idx` (`periodicnumber` ASC),
  CONSTRAINT `fk_periodictable_elements_has_star_periodictable_elements1`
    FOREIGN KEY (`periodicnumber`)
    REFERENCES `universedb`.`periodictable_elements` (`periodicnumber`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_periodictable_elements_has_star_star1`
    FOREIGN KEY (`idstar`)
    REFERENCES `universedb`.`star` (`idstar`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


INSERT INTO periodictable_elements (periodicnumber, symbol, elementname)
VALUES 
(1, "H", "Hydrogen"),
(2, "He", "Helium"),
(3, "Li", "Lithium"),
(4, "Be", "Beryllium"),
(5, "B", "Boron"),
(6, "C", "Carbon"),
(7, "N", "Nitrogen"),
(8, "O", "Oxygen"),
(9, "F", "Fluorine"),
(10, "Ne", "Neon"),
(11, "Na", "Sodium"),
(12, "Mg", "Magnesium"),
(13, "Al", "Aluminium"),
(14, "Si", "Silicon"),
(15, "P", "Phosphorus"),
(16, "S", "Sulfur"),
(17, "Cl", "Chlorine"),
(18, "Ar", "Argon"),
(19, "K", "Potassium"),
(20, "Ca", "Calcium"),
(21, "Sc", "Scandium"),
(22, "Ti", "Titanium"),
(23, "V", "Vanadium"),
(24, "Cr", "Chromium"),
(25, "Mn", "Manganese"),
(26, "Fe", "Iron"),
(27, "Co", "Cobalt"),
(28, "Ni", "Nickel"),
(29, "Cu", "Copper"),
(30, "Zn", "Zinc"),
(31, "Ga", "Gallium"),
(32, "Ge", "Germanium"),
(33, "As", "Arsenic"),
(34, "Se", "Selenium"),
(35, "Br", "Bromine"),
(36, "Kr", "Krypton"),
(37, "Rb", "Rubidium"),
(38, "Sr", "Strontium"),
(39, "Y", "Yttrium"),
(40, "Zr", "Zirconium"),
(41, "Nb", "Niobium"),
(42, "Mo", "Molybdenum"),
(43, "Tc", "Technetium"),
(44, "Ru", "Ruthenium"),
(45, "Rh", "Rhodium"),
(46, "Pd", "Palladium"),
(47, "Ag", "Silver"),
(48, "Cd", "Cadmium"),
(49, "In", "Indium"),
(50, "Sn", "Tin"),
(51, "Sb", "Antimony"),
(52, "Te", "Tellurium"),
(53, "I", "Iodine"),
(54, "Xe", "Xenon"),
(55, "Cs", "Caesium"),
(56, "Ba", "Barium"),
(57, "La", "Lanthanum"),
(58, "Ce", "Cerium"),
(59, "Pr", "Praseodymium"),
(60, "Nd", "Neodymium"),
(61, "Pm", "Promethium"),
(62, "Sm", "Samarium"),
(63, "Eu", "Europium"),
(64, "Gd", "Gadolinium"),
(65, "Tb", "Terbium"),
(66, "Dy", "Dysprosium"),
(67, "Ho", "Holmium"),
(68, "Er", "Erbium"),
(69, "Tm", "Thulium"),
(70, "Yb", "Ytterbium"),
(71, "Lu", "Lutetium"),
(72, "Hf", "Hafnium"),
(73, "Ta", "Tantalum"),
(74, "W", "Tungsten"),
(75, "Re", "Rhenium"),
(76, "Os", "Osmium"),
(77, "Ir", "Iridium"),
(78, "Pt", "Platinum"),
(79, "Au", "Gold"),
(80, "Hg", "Mercury"),
(81, "Tl", "Thallium"),
(82, "Pb", "Lead"),
(83, "Bi", "Bismuth"),
(84, "Po", "Polonium"),
(85, "At", "Astatine"),
(86, "Rn", "Radon"),
(87, "Fr", "Francium"),
(88, "Ra", "Radium"),
(89, "Ac", "Actinium"),
(90, "Th", "Thorium"),
(91, "Pa", "Protactinium"),
(92, "U","Uranium"),
(93, "Np", "Neptunium"),
(94, "Pu", "Plutonium"),
(95, "Am", "Americium"),
(96, "Cm", "Curium"),
(97, "Bk", "Berkelium"),
(98, "Cf", "Californium"),
(99, "Es", "Einsteinium"),
(100, "Fm", "Fermium"),
(101, "Md", "Mendelevium"),
(102, "No", "Nobelium"),
(103, "Lr", "Lawrencium"),
(104, "Rf", "Rutherfordium"),
(105, "Db", "Dubnium"),
(106, "Sg", "Seaborgium"),
(107, "Bh", "Bohrium"),
(108, "Hs", "Hassium"),
(109, "Mt", "Meitnerium"),
(110, "Ds", "Darmstadtium"),
(111, "Rg", "Roentgenium"),
(112, "Cn", "Copernicium"),
(113, "Nh", "Nihonium"),
(114, "Fl", "Flerovium"),
(115, "Mc", "Moscovium"),
(116, "Lv", "Livermorium"),
(117, "Ts", "Tennessine"),
(118, "Og", "Oganesson");

INSERT INTO filamenttypes (subname)
VALUES
("Galaxy Wall"),
("Large Quasar Group"),
("Supercluster complex");

INSERT INTO galaxytype (galaxytypename)
VALUES
("Elliptical galaxies"),
("Spiral galaxies"),
("Lenticular galaxies");

INSERT INTO startype (typename)
VALUES
("Protostar"),
("T Tauri Star"),
("Main Sequence Star"),
("Red Giant Star"),
("White Dwarf Star"),
("Red Dwarf Star"),
("Neutron Stars"),
("Supergiant Stars");

INSERT INTO nebulatype (nebulatypename)
VALUES
("Emission nebula"),
("Reflection nebula"),
("Dark nebula"),
("Planetary nebula"),
("Supernova remnant");

INSERT INTO orbitalbody_type (typename)
VALUES
-- Planet
("Gas dwarf"),
("Gas giant"),
("Ice giant"),
("Ice planet"),
("Iron planet"),
("Protoplanet"),
("Silicate planet"),
("Terrestrial planet "),
("Rogue planet"),
("Circumbinary planet"),
--
("Moon"),
-- Asteroid
("C-type Asteroid"), -- carbonaceous
("S-Type Asteroid"), -- silicaceous
("M-Type Asteroid"), -- metallic
-- Comet
("Non-periodic comet"),-- https://en.wikipedia.org/wiki/List_of_hyperbolic_comets https://en.wikipedia.org/wiki/List_of_near-parabolic_comets
("Long-period comet"),-- https://en.wikipedia.org/wiki/List_of_long-period_comets
("Short period comet"),-- https://en.wikipedia.org/wiki/List_of_periodic_comets#List_of_unnumbered_Jupiter-Family_comets https://en.wikipedia.org/wiki/List_of_Halley-type_comets
("Sungrazing comet"),-- https://en.wikipedia.org/wiki/List_of_Kreutz_Sungrazers
("Man Made Object"),
("Dwarf Planet");

INSERT INTO filament (Filamentname, idfilamenttypes)
VALUES
("Centaurus Great Wall", 1),
("CfA2 Great Wall", 1),
("Clowes–Campusano LQG", 2),
("Pisces–Cetus Supercluster Complex", 3)
;

INSERT INTO supercluster (idfilament,clustername,lengthmly)
VALUES
(1, "Laniakea Supercluster", 500),
(2, "Coma Supercluster", 20)
;

INSERT INTO galaxygroup (idsupercluster,galaxygroupname)
VALUES
(1, "Local Group"),
(1, "El Gordo"),
(1, "NGC 7331 Group"),
(1, "Leo Triplet")
;

INSERT INTO galaxy (idgalaxygroup,idgalaxytype,galaxyname,center,diameterkly)
VALUES
(1, 2, "Milky Way", "Sagittarius A", 180),
(1, 2, "Andromeda Galaxy", "Random Blackhole", 220),
(1, 2, "Triangulum Galaxy", "Random Blackhole 2", 60),
(1, 1, "Messier 32", "Random Blackhole 3", 7)
;

INSERT INTO solarsystem (idgalaxy,systemname,agebiy,diameterau)
VALUES
(1, "The Solal System", 4.568,  1921.56), -- Diametersolarsystem source: http://www.universetoday.com/15585/diameter-of-the-solar-system/
(1, "Alpha Centauri", 5.5,  2000)
; 

-- EARTH SOLAR SYSTEM START

INSERT INTO star (idsolarsystem,idstartype,starname,mass,luminosity,temperature,agebiy,diameter)
VALUES
(1, 3, "Sol", (1.989e+30), (3.828e+26), 5778, 4.6, 1391400),
(2, 3, "Alpha Centauri A", (2.188E+30), (1.519*(3.828e+26)), 5790, 6.45, 1708000),
(2, 3, "Alpha Centauri B", (0.907*(1.99e+30)), (0.5002*(3.828e+26)), 5260, 6.45, 1391400/2*0.865*2)
;

-- star orbitals
INSERT INTO orbitalbody (idstar, idorbitalbody_type, bodyname, mass, diameter, au_to_parent)
VALUES
(1,7, "Mercury", 3.3011e+23, 4879, 0.4),
(1,7, "Venus", 4.8675e+24, 12104, 0.7),
(1,7, "Earth", 5.97237e+24, 12742, 1),
(1,7, "Mars", 6.4171e+23, 6779, 1.5),
(1,2, "Jupiter", 1.8986e+27, 139822,5.2),
(1,2, "Saturn", 5.6836e+26 , 116464,9.5),
(1,3, "Uranus", 8.68103e+25, 50724,19.2),
(1,3, "Neptune", 1.024E26, 49244, 30.1),
(1,6, "Ceres", 8.96e+20, 950, 2.77),
(1,20, "Pluto", 1.30900e+22, 2374, 39.5)
;

-- moons
INSERT INTO orbitalbody (parent_idorbitalbody, idorbitalbody_type, bodyname, mass, diameter, km_to_parent)
VALUES
-- earth
(3, 11, "The Moon", 7.34767309e+22, 3474, 356400),

-- mars
(4, 11, "Phobos", 1.0659e+16 , 22, 9376), -- http://space-facts.com/phobos/
(4, 11, "Deimos", 1.4762e+15, 12.4, 23458), -- http://space-facts.com/deimos/

-- jupiter
(5,11,"Io", 8.9e+22, 3643, 421700),
(5,11,"Europa", 4.8e+22, 3122, 671034),
(5,11,"Ganymede", 14.8e+22, 5262, 1070412),
(5,11,"Callisto", 10.8e+22, 4821, 1882709),

-- saturn has more moons than this but these are the ones that have spherical form
(6,11,"Titan", 1.35e+23, 5149.4, 1221865),
(6,11,"Enceladus",1.08e+20, 504.2, 238037),
(6,11,"Mimas",3.80e+19,396,185520),
(6,11,"Iapetus",1.81e+21,1471.2, 3560851),
(6,11,"Dione",1.05e+21,1123, 377400),
(6,11,"Rhea",2.31e+21, 1528.6, 527068),
(6,11,"Tethys",7.55e+20, 1060, 294660),

-- Uranus
(7, 11, "Titania", 3.49e+21, 1578 , 435840),
(7, 11, "Miranda", 6.33e+19,  470, 129780),
(7, 11, "Umbriel", 1.27e+21,  1158 , 265970),
(7, 11, "Ariel", 1.27e+21,  1158 , 191240),
(7, 11, "Trinculo", 3.9e+15,  10, 8505200),
(7, 11, "Oberon", 3.03e+21, 1523, 582600),

-- Neptune
(8,11,"Triton", 2.147e+22, 2700 , 354800),

-- Pluto
(10,11,"Charon", 1.55e+21,1207.2, 19596)
; 

-- EARTH SOLAR SYSTEM END

INSERT INTO atmosphere VALUES
-- Earths atmosphere
(7,3,78.08),
(8,3,20.95),
(18,3,0.934),
(2,3,0.000542),
(36,3,0.000114),
(1,3,0.000055)
;

INSERT INTO orbitalbody_elements VALUES
-- earths crust
(8,3,46.6),
(14,3, 27.72),
(13,3,8.13),
(26,3,5),
(20,3,3.63),
(11,3,2.83),
(19,3,2.59),
(12,3, 2.09),
(22,3,0.44),
(1,3, 0.14),
(15,3, 0.12),
(25,3, 0.1),
(9,3, 0.08),
(56,3,0.05),
(6,3,0.03),
(16,3,0.05),
(23,3,0.01),
(17,3,0.05),
(24,3,0.01),
(37,3,0.03),
(29,3,0.01),
(7,3,0.005)
;

INSERT INTO star_elements VALUES
-- The sun elements
(1,1,71),
(2,1,27.1),
(8,1,0.97),
(6,1,0.4),
(7,1,0.096),
(14,1,0.099),
(12,1,0.076),
(10,1,0.058),
(29,1,0.14),
(16,1,0.04)
;

INSERT INTO nebula (idgalaxy,idnebulatype,nebulaname) 
VALUES
(1,1,"Eagle Nebula"),
(1,2,"Orion Nebula")
;

INSERT INTO nebula_elements (periodicnumber,idnebula,percentile)
VALUES

-- Orion Nebula
(1,2,10), -- hydrogen
(8,2,10), -- oxygen
(14,2,10), -- silicon
(6,2,10), -- carbon
(15,2,10), -- phosphorus
(13,2,10), -- aluminium
(7,2,10)  -- nitrogen
;

CREATE VIEW earth_atmosphere AS
SELECT atmosphere.periodicnumber, symbol, elementname, percentile
FROM atmosphere 
INNER JOIN periodictable_elements ON 
atmosphere.periodicnumber = periodictable_elements.periodicnumber 
WHERE (idorbitalbody = 3) 
ORDER BY percentile DESC
;

CREATE VIEW orion_nebula_details AS
SELECT nebula_elements.periodicnumber, symbol, elementname, percentile
FROM nebula_elements 
INNER JOIN periodictable_elements ON 
nebula_elements.periodicnumber = periodictable_elements.periodicnumber 
WHERE (idnebula = 2) 
ORDER BY percentile DESC
;

CREATE VIEW orbitalbodies_with_solparent AS
SELECT typename, bodyname, orbitalbody.mass, orbitalbody.diameter, au_to_parent, starname
FROM orbitalbody
INNER JOIN orbitalbody_type ON
orbitalbody_type.idorbitalbody_type = orbitalbody.idorbitalbody_type
INNER JOIN star ON
orbitalbody.idstar = star.idstar
WHERE orbitalbody.idstar = 1
ORDER BY au_to_parent ASC
;

CREATE VIEW elements_in_sol AS
SELECT star_elements.periodicnumber, symbol, elementname, percentile
FROM star_elements 
INNER JOIN periodictable_elements ON 
star_elements.periodicnumber = periodictable_elements.periodicnumber 
WHERE (idstar = 1) 
ORDER BY percentile DESC
;

CREATE VIEW elements_in_earth_crust AS
SELECT orbitalbody_elements.periodicnumber, symbol, elementname, percentile
FROM orbitalbody_elements 
INNER JOIN periodictable_elements ON 
orbitalbody_elements.periodicnumber = periodictable_elements.periodicnumber 
WHERE (idorbitalbody = 3) 
ORDER BY percentile DESC
;

CREATE VIEW moons_of_sol AS
SELECT typename, child.bodyname, child.mass, child.diameter, child.km_to_parent, parent.bodyname as parent
FROM orbitalbody as child
INNER JOIN orbitalbody_type ON
orbitalbody_type.idorbitalbody_type = child.idorbitalbody_type
INNER JOIN orbitalbody as parent ON
parent.idorbitalbody = child.parent_idorbitalbody
WHERE child.idorbitalbody_type = 11
ORDER BY child.mass DESC
;

SELECT * FROM atmosphere;
SELECT COUNT(periodicnumber) FROM atmosphere;

SELECT * FROM filament;
SELECT COUNT(idfilament) AS 'number of filaments' FROM filament;

SELECT * FROM filamenttypes;
SELECT COUNT(idfilamenttypes) AS 'number of filamentypes' FROM filamenttypes;

SELECT * FROM galaxy;
SELECT COUNT(idgalaxy) AS 'number of galaxies' FROM galaxy;

SELECT * FROM galaxygroup;
SELECT COUNT(idgalaxygroup) AS 'number of galaxygroups' FROM galaxygroup;

SELECT * FROM galaxytype;
SELECT COUNT(idgalaxytype) AS 'number of galaxytypes' FROM galaxytype;

SELECT * FROM nebula;
SELECT COUNT(idnebula) AS 'number of nebulas' FROM nebula;

SELECT * FROM nebula_elements;
SELECT COUNT(periodicnumber) AS 'number of nebula_elements' FROM nebula_elements;

SELECT * FROM nebulatype;
SELECT COUNT(idnebulatype) AS 'number of nebulatypes' FROM nebulatype;

SELECT * FROM orbitalbody;
SELECT COUNT(idorbitalbody) AS 'number of orbitalbodys' FROM orbitalbody;

SELECT * FROM orbitalbody_elements;
SELECT COUNT(periodicnumber) AS 'number of orbitalbody_elementss' FROM orbitalbody_elements;

SELECT * FROM orbitalbody_type;
SELECT COUNT(idorbitalbody_type) AS 'number of orbitalbody_types' FROM orbitalbody_type;

SELECT * FROM periodictable_elements;
SELECT COUNT(periodicnumber) AS 'number of periodictable_elementss' FROM periodictable_elements;

SELECT * FROM solarsystem;
SELECT COUNT(idsolarsystem) AS 'number of solarsystems' FROM solarsystem;

SELECT * FROM star;
SELECT COUNT(idstar) AS 'number of stars' FROM star;

SELECT * FROM star_elements;
SELECT COUNT(periodicnumber) AS 'number of star_elements' FROM star_elements;

SELECT * FROM startype;
SELECT COUNT(idstartype) AS 'number of startypes' FROM startype;

SELECT * FROM supercluster;
SELECT COUNT(idsupercluster) AS 'number of superclusters' FROM supercluster;

SELECT * FROM earth_atmosphere;
SELECT COUNT(periodicnumber) AS 'number of elements in earths atmosphere' FROM earth_atmosphere;

SELECT * FROM elements_in_earth_crust;
SELECT COUNT(periodicnumber) AS 'number of elements in earths crust' FROM elements_in_earth_crust;

SELECT * FROM elements_in_sol;
SELECT COUNT(periodicnumber) AS 'number of elements in suns core' FROM elements_in_sol;

SELECT * FROM moons_of_sol;
SELECT COUNT(bodyname) AS 'number of moons in solar system' FROM moons_of_sol;

SELECT * FROM orbitalbodies_with_solparent;
SELECT COUNT(bodyname) AS 'number of orbital bodys in suns orbit' FROM orbitalbodies_with_solparent;

SELECT * FROM orion_nebula_details;
SELECT COUNT(periodicnumber) AS 'number of elements in orion nebula' FROM orion_nebula_details;






