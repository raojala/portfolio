//BUG:: Apple spawns inside body
//TODO::Timer
//TODO::Add time block
//TODO::snake faster over time
//TODO::slow down snake block
//TODO::cut tail block (points per block cut)
//TODO::Score multiplier from the time survived.
/*TODO::Turn input buffer size of 2 (sometimes when moving fast the second input 
	doesn't take because step has already been taken)*/
/*TODO::Make a number matrix of the possible positions the body parts can have and maintain a list of occupied slots
	and use the unoccupied slots as range for randoms.
*/

var game = new Phaser.Game(832, 624, Phaser.AUTO, 'gameWrapper', { preload: preload, create: create, update: update });

var	timeStepMillis = 50; // change timeStep to scale difficulty
var	lastStep; // time of the last timestep
var direction;
var	directionTaken; // prevent turning 180 by allowing change only once a timestep
var	score;
var	scoreText;
var	apple;
var	bodyPieceWidth; // needed for grid calculations
var	body = new Array();

function preload() {

	game.stage.backgroundColor = "#31a2f2"; // blue

	// sprites
	game.load.crossOrigin = 'anonymous';
    game.load.image('body', 'Matopeli/matopala.png');
	game.load.image('apple', 'Matopeli/omena.png');
	
	zeroGlobals();
}

function zeroGlobals()
{
	// reset our variables
	direction = "";
	directionTaken = false;
	score = 0;
	body.splice(0, body.length);
	lastStep = 0;
}

function create()
{
    game.physics.startSystem(Phaser.Physics.ARCADE);
	
	bodyPieceWidth = game.cache.getImage('body').width;
	
	scoreText = game.add.text(5, 5, score);

    apple = game.add.sprite(-50, -50, 'apple');
	game.physics.enable(apple, Phaser.Physics.ARCADE);

	placeApple()
    addBodyPiece();

	// place the head in center of the game canvas
	body[0].x = game.width / 2 - bodyPieceWidth/2;
	body[0].y = game.height / 2;

    setInput();
}

function placeApple()
{
	// generate new position for the apple. Maximum random value is games width (or height) divided by the bodyPieceWidth
	// this way we create an imaginary grid on canvas that matches the possible positions of the snake body parts.
	// these positions are then multiplied by the bodyPieceWidth after which we remove a bodyPieceWidth once to make
	// the positions match properly.
	var applePosX = game.rnd.integerInRange(1, game.width/bodyPieceWidth);
	var applePosY = game.rnd.integerInRange(1, game.height/bodyPieceWidth);

    // reposition apple
    apple.x = (applePosX*bodyPieceWidth-bodyPieceWidth);
    apple.y = (applePosY*bodyPieceWidth-bodyPieceWidth);
}

function addBodyPiece()
{
	// make a new body part
    var bodyPart = game.add.sprite(-10,-10, 'body');

	// move the image origin to its center
    bodyPart.anchor.setTo(0.5);

	// add the physics for collisions
	game.physics.enable(bodyPart, Phaser.Physics.ARCADE);

	body.push(bodyPart);
}

function setInput()
{
	// Register the keys.
	var leftKey = game.input.keyboard.addKey(Phaser.Keyboard.LEFT);
	var rightKey = game.input.keyboard.addKey(Phaser.Keyboard.RIGHT);
	var upKey = game.input.keyboard.addKey(Phaser.Keyboard.UP);
	var downKey = game.input.keyboard.addKey(Phaser.Keyboard.DOWN);
	var pauseKey = game.input.keyboard.addKey(Phaser.Keyboard.ESC);

    // Stop the following keys from propagating up to the browser
    game.input.keyboard.addKeyCapture([
        Phaser.Keyboard.LEFT,
        Phaser.Keyboard.RIGHT,
        Phaser.Keyboard.UP,
		Phaser.Keyboard.DOWN,
		Phaser.Keyboard.ESC,
	]);
	

	// add event listeners to keys to call the functions when they're pressed
	leftKey.onDown.add(inputLeft, this);
	rightKey.onDown.add(inputRight, this);
	upKey.onDown.add(inputUp, this);
	downKey.onDown.add(inputDown, this);
	pauseKey.onDown.add(pauseGame, this);
}

function inputLeft()
{
	if (directionTaken === false && game.paused === false)
	{
		// check that we arent trying to make 180 in one move
		// also check that the key pressed isn't the same as the way
		// we're going, so we don't waste a move on a non-move.
		if (direction != "right" && direction != "left")
		{
			direction = "left";
			directionTaken = true;
		}
	}
}

function inputRight()
{
	if (directionTaken === false && game.paused === false)
	{
		if (direction != "left" && direction != "right")
		{
			direction = "right";
			directionTaken = true;
		}
	}
}

function inputUp()
{
	if (directionTaken === false && game.paused === false)
	{
		if (direction != "down" && direction != "up")
		{
			direction = "up";
			directionTaken = true;
		}
	}
}

function inputDown()
{
	if (directionTaken === false && game.paused === false)
	{
		if (direction != "up" && direction != "down")
		{
			direction = "down";
			directionTaken = true;
		}
	}
}

function pauseGame()
{
	game.paused = !game.paused;
}

function update() {

	collisionsCollection();

	// check the body length so game over doesn't crash the game.
	if (body.length > 0)
    	checkBoundaries();

    takeTimeStep();
}

// possible game collisions
function collisionsCollection()
{
	// enable game physics
    game.physics.arcade.collide(apple, body[0], appleHit);
    game.physics.arcade.collide(body[0], body, gameOver);
}

function appleHit()
{
	addScore();
	placeApple();
    addBodyPiece();
}

function addScore()
{
	score += 1;
	scoreText.setText(score);
}

function gameOver()
{
	zeroGlobals();

	// restart the game
	game.state.restart();
}

// Game boundaries, move snake to the opposite end of the map if boundary is exceeded.
function checkBoundaries()
{
    if (body[0].y < 0)
        body[0].y = game.height - bodyPieceWidth /2;
    if (body[0].y > game.height)
        body[0].y = 0 + bodyPieceWidth /2;
    if (body[0].x < 0)
        body[0].x = game.width - bodyPieceWidth /2;
    if (body[0].x > game.width)
        body[0].x = 0 + bodyPieceWidth /2;
}

// this is a Timestep.
// Inside the time step the snake moves 1 width of it's part.
// you can only change the heading of the snake once per timestep.
function takeTimeStep()
{
	if (game.time.now > lastStep+timeStepMillis)
    {
		lastStep = game.time.now;
		move();
        directionTaken = false;
    }
}

function move()
{
	// store the position of the first body element BECAUSE: if there is only
	// one body part, we can't first splice the array, and then tell the program
	// to move to this spot plus the bodyPieceWidth towards that direction, it would
	// cause a call on a element in array that does not exists.

	// take the last element in body array and place it to an variable.
	// remove the last part from the array and then splice it to first.

	// only move if direction is set.
	if (direction.length > 0)
	{
		var headX = body[0].x, headY = body[0].y; // save position of the old head before the splice

		var liikkuvaOsa = body[body.length-1]; // save the tail
		body.splice(body.length-1,1); // remove the tail from array
		body.splice(0,0, liikkuvaOsa); // place the tail as 0th elemnent in the array
	}

	// move the old tail piece (currently body[0] after the splicing)
	// towards the currently selected position
	switch (direction)
	{
		case 'left':
			body[0].x = headX - bodyPieceWidth;
			body[0].y = headY;
			break;
		case 'right':
			body[0].x = headX + bodyPieceWidth;
			body[0].y = headY;
			break;
		case 'up':
			body[0].x = headX;
			body[0].y = headY - bodyPieceWidth;
			break;
		case 'down':
			body[0].x = headX;
			body[0].y = headY + bodyPieceWidth;
			break;
	}
}
